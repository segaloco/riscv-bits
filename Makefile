TARGET	=	riscv64-linux-gnu-
CC		=	$(TARGET)gcc
AS		=	$(TARGET)as
LD		=	$(TARGET)ld
OBJCOPY =	$(TARGET)objcopy
CFLAGS	=	-nostdinc -fPIC

BIN		=	main.bin
ELF		=	main

LDFLAGS	=	-T link.ld -nostdlib

START	=	src/start.o

SYS		=	src/riscv.o \
			src/csr.o \
			src/trap.o \
			src/vm.o \
			src/uart.o

MAIN	=	src/main.o

OBJS	=	$(START) \
			$(SYS) \
			$(MAIN)

main.bin: $(ELF)
	$(OBJCOPY) -O binary $(ELF) $(BIN)

main: $(OBJS)
	$(LD) $(LDFLAGS) -o $(ELF) $(OBJS) $(LIBS)

clean:
	rm -rf $(BIN) $(ELF) $(OBJS)