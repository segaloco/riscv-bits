/* sxj - sx-like utility for the JH7100 debug UART XMODEM-CRC 
 *
 * Copyright 2022 Matthew Gilmore
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <limits.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define XMODEM_PACKET_SIZE  128
#define SHORT_BIT           (sizeof (short) * CHAR_BIT)

unsigned short crc16(const char buf[XMODEM_PACKET_SIZE])
{
    const short CRC_MASK = 0x1021;
    
    short n = XMODEM_PACKET_SIZE;
    unsigned short i;
    unsigned short crc;

    for (crc = 0; n > 0; n--, buf++) {
        for (i = (1 << (CHAR_BIT - 1)); i; i >>= 1) {
            char carry = crc >> (SHORT_BIT - 1);

            crc <<= 1;

            if (*buf & i)
                crc++;

            if (carry)
                crc ^= CRC_MASK;
        }
    }

    for (i = 0; i < SHORT_BIT; i++) {
        char carry = crc >> (SHORT_BIT - 1);

        crc <<= 1;

        if (carry)
            crc ^= CRC_MASK;
    }

    return crc;
}

size_t xmodem_crc_send(const char *buf, size_t buf_siz)
{
    const char SOH = 1, EOT = 4, ACK = 6, NAK = 21;
    const char XMODEM_PAD = 0xff;
    const char XMODEM_CRC_POLL = 'C';

    int i, c;
    unsigned char index = 1;
    char nak_count = 0;

    size_t packet_count;
    size_t last_packet_size;

    packet_count = (buf_siz / XMODEM_PACKET_SIZE);
    if ((last_packet_size = buf_siz % XMODEM_PACKET_SIZE))
        packet_count++;
    else
        last_packet_size = XMODEM_PACKET_SIZE;

    while ((c = getchar()) != XMODEM_CRC_POLL && c != EOF)
        ;

    for (i = 0; i < packet_count; i++, index++) {
        char packet[XMODEM_PACKET_SIZE];
        unsigned short checksum;

        if (i != packet_count - 1) {
            memcpy(packet, &buf[sizeof (packet)*i], sizeof (packet));
        } else {
            memcpy(packet, &buf[sizeof (packet)*i], last_packet_size);
            if (last_packet_size < sizeof (packet))
                memset(&packet[last_packet_size], XMODEM_PAD, sizeof (packet) - last_packet_size);
        }

        checksum = crc16(packet);

        putchar(SOH);
        putchar(index);
        putchar(~index);
        if (fwrite(packet, sizeof (*packet), sizeof (packet), stdout) != sizeof (packet)) {
            fprintf(stderr, "Packet %d failed\n", i);
            return -1;
        }
        putchar((checksum & (USHRT_MAX - UCHAR_MAX)) >> CHAR_BIT);
        putchar(checksum & UCHAR_MAX);

        c = getchar();

        if (c == NAK) {
            i--;
            index--;
            nak_count++;

            if (nak_count > 5) {
                fputs("Too many NAKs\n", stderr);
                return -1;
            }
        } else {
            nak_count = 0;
        }
    }

    putchar(EOT);

    return buf_siz;
}

int main(int argc, char *argv[])
{
    char *buf;

    size_t buf_len;
    FILE *payload;

    if (argc < 2) {
        fprintf(stderr, "Usage: %s payload\n", argv[0]);
        return EXIT_FAILURE;
    }

    if ((payload = fopen(argv[1], "rb")) == NULL) {
        fprintf(stderr, "Couldn't open %s\n", argv[1]);
        return EXIT_FAILURE;
    }

    fseek(payload, 0, SEEK_END);
    buf_len = (size_t) ftell(payload);
    rewind(payload);

    if ((buf = malloc(buf_len)) == NULL) {
        fprintf(stderr, "Couldn't request %d byte buffer\n", (int) buf_len);
        return EXIT_FAILURE;
    }

    if (fread(buf, 1, buf_len, payload) != buf_len) {
        fprintf(stderr, "Error reading %s\n", argv[1]);
        return EXIT_FAILURE;
    }

    fclose(payload);

    if (xmodem_crc_send(buf, buf_len) != buf_len) {
        fputs("Error performing XMODEM-CRC\n", stderr);
        return EXIT_FAILURE;
    }

    free(buf);

    return EXIT_SUCCESS;
}
