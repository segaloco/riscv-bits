/* riscv - RISC-V utility functions
 *
 * Copyright 2022 Matthew Gilmore
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "stdint.h"

#include "csr.h"
#include "trap.h"

/* disable machine interrupts */
void rv_mint_disable(void)
{
    _mie_set(0);
    _mip_set(0);
}

/* disable supervisor interrupts */
void rv_sint_disable(void)
{
    _sie_set(0);
    _sip_set(0);
}

/* allow unrestricted physical memory access */
void rv_mem_unprotected(void)
{
    _pmpaddr0_set(-1);
    _pmpcfg0_set(0x0F);
    _pmpcfg2_set(0);
}

/* disable memory mapping/virtual memory */
void rv_mem_unmapped(void)
{
    _satp_set(0);
}

/* ensure memory coherency in machine mode */
void rv_mmem_coherent(void)
{
    _fencei();
}

/* ensure memory coherency in supervisor mode */
void rv_smem_coherent(void)
{
    _sfence();
}

/* delegate necessary functionality to the supervisor */
void rv_supervisor_delegate(void (*tvec)(void))
{
    _medeleg_set(0x100);
    _stvec_set(tvec);
}

/* enter the given location in supervisor mode from machine or above */
void rv_supervisor_enter(void (*supervisor)(void))
{
    _mstatus_set(_mstatus_get() & ~mstatus_MPP | mstatus_MPP_S);
    _mepc_set(supervisor);
    _mret();
}

/* enter the given location in user mode from supervisor mode or above */
void rv_user_enter(void (*user)(void))
{
    _sstatus_set(_sstatus_get() & ~sstatus_SPP | sstatus_SPP_U);
    _sepc_set(user);
    _sret();
}

/* do a trap into the supervisor */
void rv_syscall(uint64_t syscall, uint64_t arg0)
{
    _ecall(syscall, arg0);
}

/* return from supervisor to the next user instruction */
void rv_syscall_ret(void)
{
    _sepc_set(_sepc_get() + sizeof (void *));
    _sret();
}