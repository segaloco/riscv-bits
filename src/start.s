/* start - CPU entrypoint
 *
 * Copyright 2022 Matthew Gilmore
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/* ------------------------------------------------------------------------- */
    .text
/* ------------------------------------------------------------------------- */
    .globl  _start
_start:
    csrw    mie, zero
    csrw    mip, zero

    li      t0, -1
    csrw    pmpaddr0, t0
    csrwi   pmpcfg0, 0x0f   /* rwx 0-pmpaddr0 */
    csrw    pmpcfg2, zero
    csrw    satp, zero
    fence.i

    li      t0, 0x100       /* delegate u-mode ecall to s-mode */
    csrw    medeleg, t0
    la      t0, _kernelvec
    csrw    stvec, t0

    csrr    t0, mstatus
    li      t1, ~0x1800
    and     t0, t0, t1
    li      t1, 0x800       /* previous mode = s-mode */
    or      t0, t0, t1
    csrw    mstatus, t0
    la      t0, main
    csrw    mepc, t0
    mret
/* ------------------------------------------------------------------------- */
