/* gpio - JH7100 general-purpose I/O
 *
 * Copyright 2022 Matthew Gilmore
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
    .ifndef     GPIO_I
    .equ        GPIO_I, 1
/* ------------------------------------------------------------------------- */
/* GPIO FMUX Output Data and Output Enable Source Signal List                */
/* Ref: STARFIVE-KH-SOC-PD-JH7100-2-01-V01.01.04-EN, 2021, p. 71             */
    .equ        UART0_PAD_DTRN, 125
    .equ        UART0_PAD_RTSN, 126
    .equ        UART0_PAD_SOUT, 127
    .equ        UART3_PAD_SOUT, 132

/* GPIO FMUX Input Data Destination Signal List                              */
/* Ref: STARFIVE-KH-SOC-PD-JH7100-2-01-V01.01.04-EN, 2021, p. 73             */
    .equ        UART0_PAD_CTSN, 63
    .equ        UART0_PAD_DCDN, 64
    .equ        UART0_PAD_DSRN, 65
    .equ        UART0_PAD_RIN,  66
    .equ        UART0_PAD_SIN,  67
    .equ        UART3_PAD_SIN,  74
/* ------------------------------------------------------------------------- */
    .endif      /* GPIO_I */
