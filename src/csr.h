/* csr - RISC-V 64 control and status registers
 *
 * Copyright 2022 Matthew Gilmore
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef CSR_H
#define CSR_H

#ifdef  __cplusplus
extern "C" {
#endif  /* __cplusplus */

#include "stdint.h"

/* Machine ISA Register                                                      */
/* Ref: RISC-V Privileged Architecture V20211203, s. 3.1.1, p. 15            */
extern uint64_t _misa_get(void);
extern void _misa_set(uint64_t misa);
#define misa_MXL    0xC000000000000000
#define misa_MXL_64 0x8000000000000000
#define misa_ext_A  0x0000000000000001  /* Atomic */
#define misa_ext_B  0x0000000000000002  /* Bit-Manipulation */
#define misa_ext_C  0x0000000000000004  /* Compressed */
#define misa_ext_D  0x0000000000000008  /* Double-precision floating-point */
#define misa_ext_F  0x0000000000000020  /* Single-precision floating-point */
#define misa_ext_H  0x0000000000000080  /* Hypervisor */
#define misa_ext_I  0x0000000000000100  /* RVxI base ISA */
#define misa_ext_J  0x0000000000000200  /* Dynamically Translated Languages */
#define misa_ext_M  0x0000000000001000  /* Integer Multiply/Divide */
#define misa_ext_N  0x0000000000002000  /* User-Level Interrupts */
#define misa_ext_P  0x0000000000008000  /* Packed-SIMD */
#define misa_ext_Q  0x0000000000010000  /* Quad-precision floating-point */
#define misa_ext_S  0x0000000000040000  /* Supervisor mode implemented */
#define misa_ext_U  0x0000000000100000  /* User mode implemented */
#define misa_ext_V  0x0000000000200000  /* Vector */
#define misa_ext_X  0x0000000000800000  /* Non-standard extensions present */

/* Machine Status Register                                                   */
/* Ref: RISC-V Privileged Architecture V20211203, s. 3.1.6, p. 20            */
extern uint64_t _mstatus_get(void);
extern void _mstatus_set(uint64_t mstatus);
#define mstatus_MIE     0x0008
#define mstatus_MPIE    0x0080
#define mstatus_MPP     0x1800
#define mstatus_MPP_M   0x1800
#define mstatus_MPP_S   0x0800
#define mstatus_MPP_U   0x0000

/* Machine Trap Vector Base Address                                          */
/* Ref: RISC-V Privileged Architecture V20211203, s. 3.1.7, p. 29            */
extern void *_mtvec_get(void);
extern void _mtvec_set(void *mtvec);

/* Machine Trap Delegation Registers                                         */
/* Ref: RISC-V Privileged Architecture V20211203, s. 3.1.8, p. 30            */
extern void _medeleg_set(uint64_t medeleg);
extern void _mideleg_set(uint64_t mideleg);

/* Machine Interrupt Registers                                               */
/* Ref: RISC-V Privileged Architecture V20211203, s. 3.1.9, p. 32            */
extern uint64_t _mip_get(void);
extern void _mip_set(uint64_t mip);
#define mip_MEIP    0x0800
#define mip_MTIP    0x0080
#define mip_MSIP    0x0008
extern uint64_t _mie_get(void);
extern void _mie_set(uint64_t mie);
#define mie_MEIE    0x0800
#define mie_MTIE    0x0080
#define mie_MSIE    0x0008

/* Machine Exception Program Counter                                         */
/* Ref: RISC-V Privileged Architecture V20211203, s. 3.1.14, p. 37           */
extern void *_mepc_get(void);
extern void _mepc_set(void *mepc);

/* Machine Cause Register                                                    */
/* Ref: RISC-V Privileged Architecture V20211203, s. 3.1.15, p. 38           */
extern uint64_t _mcause_get(void);

/* Physical Memory Protection CSRs                                           */
/* Ref: RISC-V Privileged Architecture V20211203, s. 3.7.1, p. 57            */
extern void _pmpaddr0_set(uint64_t pmpaddr0);
extern void _pmpaddr1_set(uint64_t pmpaddr1);
extern void _pmpcfg0_set(uint64_t pmpcfg0);
extern void _pmpcfg2_set(uint64_t pmpcfg2);

/* Supervisor Status Register                                                */
/* Ref: RISC-V Privileged Architecture V20211203, s. 4.1.1, p. 63            */
extern uint64_t _sstatus_get(void);
extern void _sstatus_set(uint64_t sstatus);
#define sstatus_SIE     0x0002
#define sstatus_SPP     0x0100
#define sstatus_SPP_S   0x0100
#define sstatus_SPP_U   0x0000

/* Supervisor Trap Vector Base Address                                       */
/* Ref: RISC-V Privileged Architecture V20211203, s. 4.1.2, p. 66            */
extern void *_stvec_get(void);
extern void _stvec_set(void *stvec);

/* Supervisor Interrupt Registers                                            */
/* Ref: RISC-V Privileged Architecture V20211203, s. 4.1.3, p. 66            */
extern uint64_t _sip_get(void);
extern void _sip_set(uint64_t sip);
#define sip_SEIP    0x0200
#define sip_STIP    0x0020
#define sip_SSIP    0x0002
extern uint64_t _sie_get(void);
extern void _sie_set(uint64_t sie);
#define sie_SEIE    0x0200
#define sie_STIE    0x0020
#define sie_SSIE    0x0002

/* Supervisor Exception Program Counter                                      */
/* Ref: RISC-V Privileged Architecture V20211203, s. 4.1.7, p. 69            */
extern void *_sepc_get(void);
extern void _sepc_set(void *sepc);

/* Supervisor Cause Register                                                 */
/* Ref: RISC-V Privileged Architecture V20211203, s. 4.1.8, p. 70            */
extern uint64_t _scause_get(void);
#define scause_ecall    8

/* Supervisor Address Translation and Protection Register                    */
/* Ref: RISC-V Privileged Architecture V20211203, s. 4.1.11, p. 73           */
extern uint64_t _satp_get(void);
extern void _satp_set(uint64_t satp);
#define satp_MODE       0xF000000000000000
#define satp_MODE_Bare  0x0000000000000000
#define satp_MODE_Sv39  0x8000000000000000

#ifdef  __cplusplus
}
#endif  /* __cplusplus */

#endif  /* CSR_H */