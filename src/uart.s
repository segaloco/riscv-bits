/* uart - JH7100 UART-supported functions
 *
 * Copyright 2022 Matthew Gilmore
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
    .include    "src/map.i"
    .include    "src/uart.i"
/* ------------------------------------------------------------------------- */
    .text
/* ------------------------------------------------------------------------- */
    .globl  kputchar
kputchar:
    li      t0, UART3
/*
1:
    lbu     t1, PC16550D_LSR(t0)
    andi    t1, t1, PC16550D_LSR_THRE
    beqz    t1, 1b
*/
    sb      a0, PC16550D_THR(t0)

    ret
/* ------------------------------------------------------------------------- */
    .globl  kputs
kputs:
    mv      t0, zero
    li      t1, UART3

1:
    lbu     t0, (a0)
    beqz    t0, 1f
/*
2:
    lbu     t2, PC16550D_LSR(t1)
    andi    t2, t2, PC16550D_LSR_THRE
    beqz    t2, 2b
*/
    sb      t0, PC16550D_THR(t1)
    addi    a0, a0, 1
    j       1b

1:
/*
    lbu     t2, PC16550D_LSR(t1)
    andi    t2, t2, PC16550D_LSR_THRE
    beqz    t2, 1b
*/
    li      t0, '\r'
    sb      t0, PC16550D_THR(t1)
/*
1:
    lbu     t2, PC16550D_LSR(t1)
    andi    t2, t2, PC16550D_LSR_THRE
    beqz    t2, 1b
*/
    li      t0, '\n'
    sb      t0, PC16550D_THR(t1)

    ret
/* ------------------------------------------------------------------------- */
