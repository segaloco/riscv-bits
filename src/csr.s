/* csr - RISC-V 64 control and status registers
 *
 * Copyright 2022 Matthew Gilmore
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/* ------------------------------------------------------------------------- */
    .text
/* ------------------------------------------------------------------------- */
    .globl  _misa_get, _misa_set
_misa_get:
    csrr    a0, misa
    ret
_misa_set:
    csrw    misa, a0
    ret
/* ------------------------------------------------------------------------- */
    .globl  _mstatus_get, _mstatus_set
    .globl  _sstatus_get, _sstatus_set
_mstatus_get:
    csrr    a0, mstatus
    ret
_mstatus_set:
    csrw    mstatus, a0
    ret
_sstatus_get:
    csrr    a0, sstatus
    ret
_sstatus_set:
    csrw    sstatus, a0
    ret
/* ------------------------------------------------------------------------- */
    .globl  _mtvec_get, _mtvec_set
    .globl  _stvec_get, _stvec_set
_mtvec_get:
    csrr    a0, mtvec
    ret
_mtvec_set:
    csrw    mtvec, a0
    ret
_stvec_get:
    csrr    a0, stvec
    ret
_stvec_set:
    csrw    stvec, a0
    ret
/* ------------------------------------------------------------------------- */
    .globl  _medeleg_set, _mideleg_set
_medeleg_set:
    csrw    medeleg, a0
    ret
_mideleg_set:
    csrw    mideleg, a0
    ret
/* ------------------------------------------------------------------------- */
    .globl  _mip_set, _mip_set
    .globl  _mie_get, _mie_set
_mip_get:
    csrr    a0, mip
    ret
_mip_set:
    csrw    mip, a0
    ret
_mie_get:
    csrr    a0, mie
    ret
_mie_set:
    csrw    mie, a0
    ret
/* ------------- */
    .globl  _sip_set, _sip_set
    .globl  _sie_get, _sie_set
_sip_get:
    csrr    a0, sip
    ret
_sip_set:
    csrw    sip, a0
    ret
_sie_get:
    csrr    a0, sip
    ret
_sie_set:
    csrw    sie, a0
    ret
/* ------------------------------------------------------------------------- */
    .globl  _mepc_get, _mepc_set
    .globl  _sepc_get, _sepc_set
_mepc_get:
    csrr    a0, mepc
    ret
_mepc_set:
    csrw    mepc, a0
    ret
_sepc_get:
    csrr    a0, sepc
    ret
_sepc_set:
    csrw    sepc, a0
    ret
/* ------------------------------------------------------------------------- */
    .globl  _mcause_get, _scause_get
_mcause_get:
    csrr    a0, mcause
    ret
_scause_get:
    csrr    a0, scause
    ret
/* ------------------------------------------------------------------------- */
    .globl  _satp_get, _satp_set
_satp_get:
    csrr    a0, satp
    ret
_satp_set:
    csrw    satp, a0
    ret
/* ------------------------------------------------------------------------- */
    .globl  _pmpaddr0_set, _pmpaddr1_set, _pmpcfg0_set, _pmpcfg2_set
_pmpaddr0_set:
    csrw    pmpaddr0, a0
    ret
_pmpaddr1_set:
    csrw    pmpaddr1, a0
    ret
_pmpcfg0_set:
    csrw    pmpcfg0, a0
    ret
_pmpcfg2_set:
    csrw    pmpcfg2, a0
    ret
